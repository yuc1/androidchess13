package com.example.chess;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.os.Bundle;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.EOFException;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutput;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

import board.Game;


public class MainActivity extends AppCompatActivity implements Serializable {


    static Context context;
    static ArrayList<Game> gameList = new ArrayList<Game>();
    boolean saveGame = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        MainActivity.context = this;
        try{
            readGames();

        }
        catch(IOException io){

        }
        catch(ClassNotFoundException c){

        }
    }


    /**
     * Click on new game button.
     * @param view -- view that was clicked
     *
     */
    public void newGameButtonSwitchActivity(View view){
        Intent newGameIntent = new Intent(this, NewGame.class);
        startActivity(newGameIntent);
    }

    public void replayButtonSwitchActivity(View view){
        Intent replayIntent = new Intent(this, ReplayGame.class);
        startActivity(replayIntent);


    }


    /**
     * writes users to the users.ser with serializable.
     * @throws IOException
     */
    public static void writeGame() throws IOException {
        FileOutputStream fos = context.openFileOutput("Games.ser", Context.MODE_PRIVATE);
        ObjectOutputStream os = new ObjectOutputStream(fos);
        os.writeObject(gameList);
        os.close();
        fos.close();
    }


    /**
     * reads user from the Users.ser file with serializable
     * @throws IOException
     * @throws ClassNotFoundException
     */
    public static void readGames() throws IOException, ClassNotFoundException{
        Log.e("====== READING SER MAIN ==========>", "READING IN SER MAIN");

        try {


            FileInputStream fileInputStream = context.openFileInput("Games.ser");
            //System.out.println("read");
            ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
            gameList = (ArrayList<Game>)objectInputStream.readObject();
            //Log.e("ARRAYLIST MADE", "GAMELIST SHOULDNT BE FUCKING EMPTY");
            objectInputStream.close();
            fileInputStream.close();

        }
        //nothing in file
        catch(FileNotFoundException f){

            System.out.println("file not found");
        }
        catch(EOFException e) {
            Log.e("MAKE", "MAKING ARRAYLIST");
            gameList = new ArrayList<Game>();

        }

    }







}
