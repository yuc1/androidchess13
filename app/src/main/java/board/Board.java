
package board;

import android.util.Log;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import board.PromotedPiece;

import Pieces.*;


/**
 * @author il166, yuc1
 * Board class contains the chess board and all helper methods.
 */
public class Board {

    public static ChessPiece[][] board = new ChessPiece[8][8];

    private static King whiteKing;
    private static King blackKing;
    private static ArrayList<ChessPiece> blackPieces = new ArrayList<>();
    public static ArrayList<ChessPiece> whitePieces = new ArrayList<>();
    private static boolean movedTwo = false;
    private static boolean enPassant = false;
    private static ChessPiece previousPiece;
    public static boolean printCastle = false;
    public static String rookLocation="";
    public static String rookinit="";
    public static Hashtable<Integer, ChessPiece> table = new Hashtable<>();
    public static int moveCounter = 0;
    public static boolean enpass=false;
    public static boolean castling = false;
    public static ArrayList<PromotedPiece> promotedPieces = new ArrayList<>();

    public Board() {
        blackPieces.clear();
        whitePieces.clear();
        board = new ChessPiece[8][8];
        for (int i = 0; i < 8; i++) {
            board[1][i] = new Pawn('b', 1, i);
            blackPieces.add(board[1][i]);

        }

        for (int i = 0; i < 8; i++) {
            board[6][i] = new Pawn('w', 6, i);
            whitePieces.add(board[6][i]);

        }

        board[0][0] = new Rook('b', 0, 0);
        board[0][1] = new Knight('b', 0, 1);
        board[0][2] = new Bishop('b', 0, 2);
        board[0][3] = new Queen('b', 0, 3);
        board[0][4] = new King('b', 0, 4);
        board[0][7] = new Rook('b', 0, 7);
        board[0][6] = new Knight('b', 0, 6);
        board[0][5] = new Bishop('b', 0, 5);

        blackKing = (King) board[0][4];
        blackPieces.add(board[0][0]);
        blackPieces.add(board[0][1]);
        blackPieces.add(board[0][2]);
        blackPieces.add(board[0][3]);
        blackPieces.add(board[0][4]);
        blackPieces.add(board[0][5]);
        blackPieces.add(board[0][6]);
        blackPieces.add(board[0][7]);


        board[7][0] = new Rook('w', 7, 0);
        board[7][1] = new Knight('w', 7, 1);
        board[7][2] = new Bishop('w', 7, 2);
        board[7][3] = new Queen('w', 7, 3);
        board[7][4] = new King('w', 7, 4);
        board[7][7] = new Rook('w', 7, 7);
        board[7][6] = new Knight('w', 7, 6);
        board[7][5] = new Bishop('w', 7, 5);

        whiteKing = (King) board[7][4];
        whitePieces.add(board[7][0]);
        whitePieces.add(board[7][1]);
        whitePieces.add(board[7][2]);
        whitePieces.add(board[7][3]);
        whitePieces.add(board[7][4]);
        whitePieces.add(board[7][5]);
        whitePieces.add(board[7][6]);
        whitePieces.add(board[7][7]);
        moveCounter = 0;

        printBoard(board);
    }

    /**
     * Prints the board.
     *
     * @param boardToPrint 2d array of the board to be printed. .
     */
    public static void printBoard(ChessPiece[][] boardToPrint) {
        for (int i = 0; i < 8; i++) {
            for (int j = 0; j < 8; j++) {

                if (boardToPrint[i][j] == null && ((j % 2 == 1 && i % 2 == 0) || (j % 2 == 0 && i % 2 == 1))) {
                    System.out.print("## ");
                } else if (boardToPrint[i][j] != null) {
                    System.out.print(boardToPrint[i][j] + " ");

                } else {
                    System.out.print("   ");
                }

                if (j == 7) {
                    System.out.print(" " + (8 - i));
                    System.out.println();
                }

            }
        }
        System.out.println(" a  b  c  d  e  f  g  h");
        System.out.println();
    }

    /**
     * Gets the piece at a location of the board.
     *
     * @param location of the board
     * @return ChessPiece Object from that location. will be null if empty.
     */
    public static ChessPiece getPieceAtLocation(int[] location) {


        return board[location[0]][location[1]];
    }


    /**
     * Translates the string input to a int array corresponding to the board.
     *
     * @param chessColumnRow
     * @return int array of corresponding spot on board
     */

    public static int[] translateToArray(String chessColumnRow) {
        char[] colRow = chessColumnRow.toCharArray();

        int[] translatedColRow = new int[2];

        translatedColRow[0] = Character.getNumericValue(colRow[0]);
        translatedColRow[1] = Character.getNumericValue(colRow[1]);

        return translatedColRow;

    }


    /**
     * Checks of the move inputed by the user is a valid move. Also checks for castling and EN Passant.
     *
     * @param origin      - Chess piece to be moved
     * @param destination String format for the destination.
     * @return boolean to see if move is valid
     */
    public static boolean checkMoveValidity(ChessPiece origin, String destination) {

        if (origin == null)
            return false;

        ArrayList<int[]> moves = origin.validMoves();

        if (moves.size() == 0) {
            return false;
        }
        int[] dest = translateToArray(destination);

        char color = origin.getColor();


        if (origin instanceof King && origin.getColor() == 'w') {
            King whiteK = (King) origin;
            if (whiteK.moveCounter == 0) {
                int[] pos = whiteK.getPosition();
                int row = pos[0];
                int column = pos[1];
                //castle right
                if (destination.equals("76") || destination.equals("72")) {
                    if (row == 7 && column == 4) {
                        if (!Board.isKingChecked('w', Board.board)) {
                            //System.out.println("CASTLING");

                            //check left rook
                            boolean castleLeft = true;
                            boolean castleRight = true;
                            for (int i = column - 1; i > 0; i--) {
                                if (Board.getPieceAtLocation(new int[]{row, i}) != null) {
                                    castleLeft = false;
                                    break;
                                }
                            }
                            //check right rook
                            for (int i = column + 1; i < 7; i++) {
                                if (Board.getPieceAtLocation(new int[]{row, i}) != null) {
                                    castleRight = false;
                                    break;
                                }
                            }


                            if (castleLeft) {

                                //System.out.println("CASTLING LEFT");
                                //check left rook is still there and add
                                if (Board.getPieceAtLocation(new int[]{row, 0}) != null && Board.getPieceAtLocation(new int[]{row, 0}) instanceof Rook && Board.getPieceAtLocation(new int[]{row, 7}).getColor() == 'w') {
                                    if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 3}) == true) {
                                        if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 2}) == true) {
                                            return true;
                                        }
                                    }
                                }

                            }


                            if (castleRight) {
                                //check right rook is still there and add
                                //System.out.println("CASTLE RIGHT");
                                if (Board.getPieceAtLocation(new int[]{row, 7}) != null && Board.getPieceAtLocation(new int[]{row, 7}) instanceof Rook && Board.getPieceAtLocation(new int[]{row, 7}).getColor() == 'w') {
                                    if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 5}) == true) {
                                        if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 6}) == true) {
                                            return true;
                                        }
                                    }
                                }
                            }

                        }

                    }
                }
            }
        }

        if (origin instanceof King && origin.getColor() == 'b') {
            //System.out.println("black king");
            King blackK = (King) origin;
            if (blackK.moveCounter == 0) {
                //System.out.println("black counter 0");
                int[] pos = blackK.getPosition();
                int row = pos[0];
                int column = pos[1];
                //castle right
                if (destination.equals("06") || destination.equals("02")) {
                    if (row == 0 && column == 4) {
                        if (!Board.isKingChecked('b', Board.board)) {
                            //System.out.println("CASTLING");

                            //check left rook
                            boolean castleLeft = true;
                            boolean castleRight = true;
                            for (int i = column - 1; i > 0; i--) {
                                if (Board.getPieceAtLocation(new int[]{row, i}) != null) {
                                    castleLeft = false;
                                    break;
                                }
                            }
                            //check right rook
                            for (int i = column + 1; i < 7; i++) {
                                if (Board.getPieceAtLocation(new int[]{row, i}) != null) {
                                    castleRight = false;
                                    break;
                                }
                            }


                            if (castleLeft) {

                                //System.out.println("CASTLING LEFT");
                                //check left rook is still there and add
                                if (Board.getPieceAtLocation(new int[]{row, 0}) != null && Board.getPieceAtLocation(new int[]{row, 0}) instanceof Rook && Board.getPieceAtLocation(new int[]{row, 7}).getColor() == 'b') {
                                    if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 3}) == true) {
                                        if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 2}) == true) {
                                            return true;
                                        }
                                    }
                                }

                            }


                            if (castleRight) {
                                //check right rook is still there and add
                                //System.out.println("CASTLE RIGHT");
                                if (Board.getPieceAtLocation(new int[]{row, 7}) != null && Board.getPieceAtLocation(new int[]{row, 7}) instanceof Rook && Board.getPieceAtLocation(new int[]{row, 7}).getColor() == 'b') {
                                    if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 5}) == true) {
                                        if (Board.testMove(Board.getPieceAtLocation(new int[]{row, 4}), new int[]{row, 6}) == true) {
                                            return true;
                                        }
                                    }
                                }
                            }

                        }

                    }
                }

            }
        }


        //En passant
        if (movedTwo && origin instanceof Pawn) {
            //System.out.println("inside");

            int[] originLocation = origin.getPosition();

            if (color == 'w') {
                if (originLocation[0] - 1 == dest[0]
                        && (originLocation[1] + 1 == dest[1] || originLocation[1] - 1 == dest[1])
                        && (previousPiece.equals(board[3][dest[1]]))
                        && (originLocation[0] == 3 && board[2][originLocation[1]] == null && board[1][originLocation[1]] != null)) {
                    enPassant = true;
                    movedTwo = false;
                    boolean returnval = testMove(origin, dest);
                    if (!returnval) {
                        movedTwo = true;
                        enPassant = false;
                    }
                    return returnval;
                }
            } else if (color == 'b') {
                if (originLocation[0] + 1 == dest[0]
                        && (originLocation[1] + 1 == dest[1] || originLocation[1] - 1 == dest[1])
                        && (previousPiece.equals(board[4][dest[1]]))
                        && (originLocation[0] == 4 && board[5][originLocation[1]] == null && board[6][originLocation[1]] != null)) {
                    enPassant = true;
                    movedTwo = false;
                    boolean returnval = testMove(origin, dest);
                    if (!returnval) {
                        movedTwo = true;
                        enPassant = false;
                    }
                    return returnval;
                }
            }
        }


        for (int[] move : moves) {

            if (move[0] == dest[0] && move[1] == dest[1]) {

                boolean moveValid = testMove(origin, dest);
                if (origin instanceof Pawn && moveValid) {

                    if (color == 'w' && origin.getPosition()[0] - dest[0] == 2) {
                        movedTwo = true;
                    } else if (color == 'b' && origin.getPosition()[0] - dest[0] == -2) {
                        movedTwo = true;
                    } else {
                        movedTwo = false;
                    }

                }
                if (moveValid)
                    previousPiece = origin;

                return moveValid;

            }
        }

        return false;
    }


    /**
     * Uses a copy of the board, to test a move. Checks if the move is valid i.e king is in check after the move.
     *
     * @param origin - piece to be moved.
     * @param dest   - destination row and column in an array
     * @return boolean to determine if moving that piece is possible - will check if King is in check
     */
    public static boolean testMove(ChessPiece origin, int[] dest) {

        ChessPiece removedPiece = null;

        char c = 0;

        // make a copy of the board.
        ChessPiece[][] board2 = Arrays.stream(board).map(ChessPiece[]::clone).toArray(ChessPiece[][]::new);

        //get position of piece
        int[] pieceLocation = origin.getPosition();
        //move piece


        //check if location is null.
        if (board[dest[0]][dest[1]] != null) {
            removedPiece = board[dest[0]][dest[1]];
            c = origin.getColor();
            if (c == 'b') {
                whitePieces.remove(removedPiece);
            } else {
                blackPieces.remove(removedPiece);
            }
        }

        //set old location to null
        board[dest[0]][dest[1]] = origin;

        board[pieceLocation[0]][pieceLocation[1]] = null;

        origin.updatePosition(dest);

        //pass in board and the kingColor

        //King is checked
        if (isKingChecked(origin.getColor(), board2)) {
            //reset board to what is used to be.
            board = Arrays.stream(board2).map(ChessPiece[]::clone).toArray(ChessPiece[][]::new);
            origin.updatePosition(pieceLocation);
            if (removedPiece != null) {
                if (c == 'b') {
                    whitePieces.add(removedPiece);
                } else {
                    blackPieces.add(removedPiece);
                }
            }
            return false;

            //King is not checked.
        } else {
            board = Arrays.stream(board2).map(ChessPiece[]::clone).toArray(ChessPiece[][]::new);
            origin.updatePosition(pieceLocation);
            if (removedPiece != null) {
                if (c == 'b') {
                    whitePieces.add(removedPiece);
                } else {
                    blackPieces.add(removedPiece);
                }
            }
            return true;
        }
    }


    /**
     * Checks if the king is checked.
     *
     * @param kingColor - color of the king
     * @param board2    - clone of board to test for check
     * @return boolean to determine if king is in check
     */
    public static boolean isKingChecked(char kingColor, ChessPiece[][] board2) {

        //List of all possible moves of opposite colored pieces.
        ArrayList<int[]> possibleMoves = new ArrayList<>();

        //if black
        if (kingColor == 'b') {
            whitePieces.forEach(currentPiece -> possibleMoves.addAll(currentPiece.validMoves()));

            for (int[] move : possibleMoves) {
                if (move[0] == blackKing.getPosition()[0] && move[1] == blackKing.getPosition()[1]) {
                    return true;
                }
            }

            //if white
        } else {
            blackPieces.forEach(currentPiece -> possibleMoves.addAll(currentPiece.validMoves()));

            for (int[] move : possibleMoves) {
                if (move[0] == whiteKing.getPosition()[0] && move[1] == whiteKing.getPosition()[1]) {
                    return true;
                }
            }
        }


        return false;
    }


    /**
     * Checks for checkmate.
     *
     * @param color - color of that king
     * @return boolean to determine if that color's king is checkmated
     */
    @SuppressWarnings("unchecked")
    public static boolean isKingCheckMated(char color) {

        ArrayList<int[]> moves = null;

        ArrayList<ChessPiece> pieces = null;

        boolean check = false;
        if (color == 'b') {
            pieces = (ArrayList<ChessPiece>) whitePieces.clone();
        } else {
            pieces = (ArrayList<ChessPiece>) blackPieces.clone();

        }

        for (ChessPiece piece : pieces) {
            moves = piece.validMoves();

            for (int[] move : moves) {

                check = testMove(piece, move);
                if (check) {
                    //System.out.println(piece);
                    //System.out.println(piece.getPosition()[0] + " " + piece.getPosition()[1]);
                    return false;
                }
            }
        }
        return true;
    }

    public static ArrayList<Integer> getAValidMove(boolean whitesTurn) {
        ArrayList<Integer> moves = new ArrayList<Integer>();
        int pos = 0;
        int pos2 = 0;
        int x = 0;
        ArrayList<int[]> move = new ArrayList<int[]>();
        if (whitesTurn) {

            for (int i = 0; i < whitePieces.size(); i++) {
                move = whitePieces.get(i).validMoves();
                pos = i;
                System.out.println(whitePieces.get(i).getPosition()[0]);
                if (move.size() > 0) {
                    System.out.println("inside");
                    for (int j = 0; j < move.size(); j++) {
                        System.out.println("psojdow");
                        pos2 = j;
                        int[] test = move.get(j);
                        int po = test[0] * 10 + test[1];
                        String dest;
                        if(po<10){
                            dest="0"+Integer.toString(po);
                        }else{
                            dest=Integer.toString(po);
                        }
                        if (checkMoveValidity(getPieceAtLocation(whitePieces.get(i).getPosition()), dest)) {
                            x = 1;
                            break;
                        }
                    }
                    if (x == 1)
                        break;
                }
            }
            if (move.size() == 0)
                return null;

            int[] dest = move.get(pos2);
            int[] src = whitePieces.get(pos).getPosition();

            int destnum = dest[0] * 10 + dest[1];
            int srcnum = src[0] * 10 + src[1];
            moves.add(srcnum);
            moves.add(destnum);


        } else {
            for (int i = 0; i < blackPieces.size(); i++) {
                move = blackPieces.get(i).validMoves();
                pos = i;
                if (move.size() != 0) {
                    for (int j = 0; j < move.size(); j++) {
                        pos2 = j;
                        int[] test = move.get(j);
                        int po = test[0] * 10 + test[1];
                        String dest;
                        if(po<10){
                            dest="0"+po;
                        }else{
                            dest=Integer.toString(po);
                        }
                        Log.d("size of i", ": "+i+ " " + blackPieces.size());
                        if (checkMoveValidity(getPieceAtLocation(blackPieces.get(i).getPosition()), dest)) {
                            x = 1;
                            break;
                        }
                    }
                    if (x == 1)
                        break;
                }
            }
            if (move.size() == 0)
                return null;

            int[] dest = move.get(pos2);
            int[] src = blackPieces.get(pos).getPosition();

            int destnum = dest[0] * 10 + dest[1];
            int srcnum = src[0] * 10 + src[1];
            moves.add(srcnum);
            moves.add(destnum);


        }

        return moves;
    }


    /**
     * Moves the piece given the origin as a string and destination as a string
     *
     * @param origin      - piece that you wish to move's origin string
     * @param Destination - destination string inputed by user
     */
    public void movePiece(String origin, String Destination) {

        int[] pieceLocation = translateToArray(origin);
        int[] destination = translateToArray(Destination);



        ChessPiece pieceToMove = getPieceAtLocation(translateToArray(origin));

        if (pieceToMove instanceof King) {
            King kingOrigin = (King) pieceToMove;
            kingOrigin.moveCounter = 1;
            //white king castle case
            if (origin.equals("74")) {
                if (Destination.equals("72")) {
                    castling = true;
                    printCastle = true;
                    movePiece("70", "73");
                    rookLocation="73";
                    rookinit="70";
                } else if (Destination.equals("76")) {
                    System.out.println("I AM HERE AND YOU ARE A BIG POOPY DUMB DUMB HEAD");
                    castling = true;
                    printCastle = true;
                    movePiece("77", "75");
                    rookLocation="75";
                    rookinit="77";
                }
            }
            //black king castle case
            else if (origin.equals("04")) {
                if (Destination.equals("02")) {
                    castling = true;
                    printCastle = true;
                    movePiece("00", "03");
                    rookLocation="03";
                    rookinit="00";
                } else if (Destination.equals("06")) {
                    castling = true;
                    printCastle = true;
                    movePiece("07", "05");
                    rookLocation="05";
                    rookinit="07";
                }
            }
        }

        pieceToMove.updatePosition(destination);

        if(board[destination[0]][destination[1]]!=null) {
            ChessPiece pieceToRemove = board[destination[0]][destination[1]];
            table.put(moveCounter, pieceToRemove);
            System.out.println("REMOVED PIECE: " + pieceToRemove);
            char c= pieceToRemove.getColor();
            if(c=='b')
                blackPieces.remove(pieceToRemove);
            else
                whitePieces.remove(pieceToRemove);
        }
        board[destination[0]][destination[1]] = pieceToMove;
        board[pieceLocation[0]][pieceLocation[1]] = null;
        enpass=enPassant;
        if (enPassant) {
            ChessPiece pieceToRemove = null;

            if (pieceToMove.getColor() == 'w') {
                pieceToRemove = board[destination[0] + 1][destination[1]];
            } else {
                pieceToRemove = board[destination[0] - 1][destination[1]];
            }

            char c = pieceToRemove.getColor();
            if (c == 'b') {
                blackPieces.remove(pieceToRemove);
                board[destination[0] + 1][destination[1]] = null;
            } else {
                whitePieces.remove(pieceToRemove);
                board[destination[0] - 1][destination[1]] = null;
            }
            enPassant = false;

        }


        if (!castling) {
            printBoard(board);
        } else castling = false;

        moveCounter++;
    }


    public boolean movePieceReplay(String origin, String Destination) {


        int[] pieceLocation = translateToArray(origin);
        int[] destination = translateToArray(Destination);

        //if(){
          //  System.out.println("REPLAY PROMOTION");
            //return true;
        //}




        ChessPiece pieceToMove = getPieceAtLocation(translateToArray(origin));


        pieceToMove.updatePosition(destination);
        if (board[destination[0]][destination[1]] != null) {
            ChessPiece pieceToRemove = board[destination[0]][destination[1]];
            table.put(moveCounter, pieceToRemove);
            System.out.println("REMOVED PIECE: " + pieceToRemove.toString() + ", " + moveCounter);
        }


        board[destination[0]][destination[1]] = pieceToMove;
        board[pieceLocation[0]][pieceLocation[1]] = null;

        moveCounter++;


        printBoard(board);
        //System.out.println("MOVE: " + moveCounter);
        return false;

    }


    /**
     * Checks to see if promotion is valid.
     *
     * @param origin      - origin string
     * @param destination - destination string
     * @param newPiece    - new desired piece for promotion
     * @param color       - origin color
     * @return boolean for chess13 to see if possible/actual functionality of promotion
     */
    public boolean checkPromotion(String origin, String destination, String newPiece, char color) {
        if (checkMoveValidity(getPieceAtLocation(translateToArray(origin)), destination) == true && getPieceAtLocation(translateToArray(origin)) instanceof Pawn) {
            ChessPiece promotedPiece;
            int[] d = translateToArray(destination);
            int[] pieceLocation = translateToArray(origin);
            if (color == 'w') {

                if (translateToArray(destination)[0] == 0 && translateToArray(origin)[0] == 1) {
                    if(getPieceAtLocation(translateToArray(destination)) != null) table.put(moveCounter, getPieceAtLocation(translateToArray(destination)));
                    System.out.println("REMOVED PIECE: " + getPieceAtLocation(translateToArray(origin)));
                    whitePieces.remove(getPieceAtLocation(translateToArray(origin)));
                    if (newPiece.equals("Q")) {
                        promotedPiece = new Queen(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        whitePieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;

                    } else if (newPiece.equals("R")) {
                        promotedPiece = new Rook(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        whitePieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                    } else if (newPiece.equals("B")) {
                        promotedPiece = new Bishop(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        whitePieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;

                    } else if (newPiece.equals("N")) {
                        promotedPiece = new Knight(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        whitePieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                    }

                    moveCounter++;
                    printBoard(board);

                    return true;
                } else return false;
            } else {

                if (translateToArray(destination)[0] == 7 && translateToArray(origin)[0] == 6) {
                    if(getPieceAtLocation(translateToArray(destination)) != null) table.put(moveCounter, getPieceAtLocation(translateToArray(destination)));
                    System.out.println("REMOVED PIECE: " + getPieceAtLocation(translateToArray(origin)));
                    blackPieces.remove(getPieceAtLocation(translateToArray(origin)));
                    if (newPiece.equals("Q")) {
                        promotedPiece = new Queen(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        blackPieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        //table.put(moveCounter, board[d[0]][d[1]]);
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                        System.out.println("Move counter in checkpromotion: " + moveCounter);
                        System.out.println("promoted piece in checkpromotion:" + promotedPiece);
                    } else if (newPiece.equals("R")) {
                        promotedPiece = new Rook(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        blackPieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        //table.put(moveCounter, board[d[0]][d[1]]);
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                        System.out.println("Move counter in checkpromotion: " + moveCounter);
                        System.out.println("promoted piece in checkpromotion:" + promotedPiece);
                    } else if (newPiece.equals("B")) {
                        promotedPiece = new Bishop(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        blackPieces.add(promotedPiece);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        //table.put(moveCounter, board[d[0]][d[1]]);
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                        System.out.println("Move counter in checkpromotion: " + moveCounter);
                        System.out.println("promoted piece in checkpromotion:" + promotedPiece);

                    } else if (newPiece.equals("N")) {
                        promotedPiece = new Knight(color, translateToArray(destination)[0], translateToArray(destination)[1]);
                        promotedPiece.updatePosition(translateToArray(destination));
                        promotedPieces.add(new PromotedPiece(moveCounter, promotedPiece));
                        //table.put(moveCounter, board[d[0]][d[1]]);
                        board[d[0]][d[1]] = null;
                        board[d[0]][d[1]] = promotedPiece;
                        board[pieceLocation[0]][pieceLocation[1]] = null;
                        System.out.println("Move counter in checkpromotion: " + moveCounter);
                        System.out.println("promoted piece in checkpromotion:" + promotedPiece);
                    }



                    moveCounter++;
                    printBoard(board);


                    return true;
                } else return false;
            }


        }

        return false;


    }
}